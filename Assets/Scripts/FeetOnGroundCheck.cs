﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetOnGroundCheck : MonoBehaviour
{
    public bool isGrounded;
    public LayerMask groudElements;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.IsTouchingLayers(groudElements) && isGrounded != true)
        {
            isGrounded = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.IsTouchingLayers(groudElements) && isGrounded == true)
        {
            isGrounded = false;
        }
    }
}

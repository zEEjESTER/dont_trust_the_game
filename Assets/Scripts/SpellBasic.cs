﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBasic : MonoBehaviour
{
    public GameObject projectile;
    public float minDamage;
    public float maxDamage;
    public float projectileForce;
    private void Update()
    {

        if (Input.GetMouseButtonDown(1))
        {
            GameObject spell = Instantiate(projectile, transform.position, Quaternion.identity);
            var mousePos = Input.mousePosition;
            mousePos.z = 10; // select distance = 10 units from the camera
            var mousePosV2 = Camera.main.ScreenToWorldPoint(mousePos);
            var myPos = transform.position;
            Vector2 direction = (mousePosV2 - myPos).normalized;
            Debug.Log(direction);
            spell.GetComponent<Rigidbody2D>().velocity = direction * projectileForce;
            spell.GetComponent<Projectile>().damage = Random.Range(minDamage, maxDamage);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject feet;
    public float speed;
    public float jumpHight;
    private Vector2 direction;
    public Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }
    private void Update()
    {
        TakeInput();
        
        Move();
        Jump();
    }

    private void TakeInput()
    {
        direction = Vector2.zero;
        if (Input.GetKey(KeyCode.W))
        {
            direction += Vector2.up;
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction += Vector2.left;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction += Vector2.down;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction += Vector2.right;
        }
    }

    private void Move()
    {
        transform.Translate(direction * speed * Time.deltaTime);

        if (direction.x != 0 || direction.y !=0)
        {
            if (animator.GetLayerWeight(1) != 1)
            {
                animator.SetLayerWeight(1, 1);
            }
            SetAnimatorMovement(direction);
        }
        else
        {
            if (animator.GetLayerWeight(1) != 0)
            {
                animator.SetLayerWeight(1, 0);
            }
        }
        
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (feet.GetComponent<FeetOnGroundCheck>().isGrounded)
            {
                Debug.Log("Jumping!");
                GetComponent<Rigidbody2D>().velocity += direction + Vector2.up * jumpHight;
            }
        }
    }

    private void SetAnimatorMovement(Vector2 _direction)
    {
        animator.SetFloat("xDir", _direction.x);
        animator.SetFloat("yDir", _direction.y);
    }
}
